# Test Web App using Docker Compose
### This is a simple project to test the docker web application.
### This application consists in a flask app and a redis database, which shows a web page and indicates how many times this page has been accessed.

#### Build environment:

* Docker version 17.06
* docker-compose version 1.18.0
    
#### Launch application:

1. `git clone https://kokai@bitbucket.org/kokai/docker-test-web-app.git`
2. `cd to/this/project/`
3. `docker-compose up`
4. after the application is built, open the browser and navigate to localhost:5000
5. This page will display "Hello World! This page has been visited 1 times"
6. refresh the page will increment the counts of the visiting.

#### Stop application

* use ctrl + c to terminate application
* use `docker container prune` to delete the stopped containers


